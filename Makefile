MARKDOWN_EXTENSIONS = -x abbr -x def_list -x tables
MARKDOWN = python -m markdown $(MARKDOWN_EXTENSIONS)

HEADER = header.html
FOOTER = footer.html

MARKDOWNS != find . -name '*.md'
HTMLS = $(MARKDOWNS:.md=.html)

all: $(HTMLS)

clean:
	rm $(HTMLS)

.SUFFIXES: .html .md
.md.html: $(HEADER) $(FOOTER)
	(cat $(HEADER); $(MARKDOWN) $<; cat $(FOOTER)) > $@
