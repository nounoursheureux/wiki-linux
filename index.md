# Wiki Linux

Ceci est le Wiki officiel du forum JVC Linux, n'hésitez pas à nous
signaler des erreurs, nous demander conseil ou nous suggérer des idées
ici : [le topic officiel du projet][topic-officiel].

[topic-officiel]: http://www.jeuxvideo.com/forums/1-38-7796926-1-0-1-0-projet-le-wiki-du-forum-v2.htm

LE WIKI EST EN COURS DE COMPLÉTION.

* * *

## Se lancer dans GNU/Linux

[Lexique](lexique.html)
: Utiliser et comprendre GNU/Linux nécessite un vocabulaire qui n'est
  pas inné. Vous trouverez ici les explications concernant des mots,
  expressions ou abréviations dont le sens peut vous échapper.

[Pourquoi utiliser Linux ?](http://francoisautin.free.fr/wikilinux/pmwiki.php?n=Main.PourquoiUtiliserLinux?)
: Une brève description des avantages d'une distribution Linux.

[Quelle distribution choisir ?](https://linux-test.deblan.org/) &gt;&gt; **Le questionnaire est mort ?**
: Ce questionnaire vous permettra de choisir la distribution GNU/Linux
  qui vous convient le plus.

* * *

## Installation et adaptation

[Où télécharger les images d'installation ?](telecharger-images-installation.html)
: Vous trouverez ici les liens les plus courants ainsi que les ISO
  d'installation à choisir.

[Créer une clé USB-CD-DVD-SD bootable](peripherique-bootable.html)
: La manière la plus directe de créer un clef usb bootable.
  [Version vidéo](https://www.youtube.com/watch?v=YUrmpU2HorU)

[Comment faire un dual-boot ?](dual-boot.html)
: Vous désirez installer Linux tout en continuant à avoir Windows ou OSX ?
  Avoir deux distributions Linux ? Plus ? C'est tout à fait possible,
  suivez le guide.

[Quel format de fichiers utiliser ?](systeme-fichiers.html)
: Il existe plusieurs format de fichiers sous Linux comme ailleurs, mais
  entre l'EXT, le FAT, le NTFS, le HFS+, vous êtes perdus, alors, c'est
  cet article qu'il vous faut lire.

[Dans le cadre d'un partage entre Windows et Linux ?](partage-windows-linux.html)
: Vous désirez avoir une partition commune à Windows et Linux ?

[Quel environnement choisir ?](environnement.html)
: Les environnements sont également très nombreux. Ici, nous vous
  présenterons les différentes interfaces possibles, leurs avantages comme
  leurs inconvénients, afin de vous permettre de faire votre choix parmi
  les multiples possibilités.

[Alternatives aux logiciels Windows ou OS X ?](alternatives.html)
: Linux n'est pas Windows et Linux n'est pas OSX, les logiciels qui y
  tournent sont parfois les mêmes, parfois différents, parfois très
  similaires. Vous avez vos habitudes et aimeriez passer à des logiciels
  très équivalents ? Nous vous aiguillerons au mieux.

* * *

## Les bases

[Les bases du terminal](bases-terminal.html)
: Le terminal fait peur aux novices, mais il s'avère être un outil très
  pratique, dont l'apprentissage est facile et qui vous permettra de
  gagner du temps. Vous découvrirez ici les principales actions de cet
  outil, ainsi que quelques personnalisations très sympathiques.

[Installer un logiciel de toutes les manières possibles](installer-logiciels.html)
: Installer un logiciel sous Linux est très souvent extrêmement facile.
  En installer 50 en une minute l'est tout autant. Apprenez ici à le
  faire.

[Que faire quand un logiciel plante ?](plantage.html)
: Si un logiciel plante, voici les étapes à suivre pour n'avoir aucun
  problème.

[Comment conserver sa configuration ?](conserver-configuration.html)
: Pour une nouvelle installation, ou simplement pour avoir deux
  ordinateurs avec le même comportement, vous désirez conserver toute
  votre configuration ? Nous vous expliquons la logique et quelques
  procédures possibles.

[Comment sauvegarder son OS ?](sauvegarder-systeme)
: La prudence est souvent à privilégier en informatique, et faire des
  sauvegardes est toujours conseillé. Il existe plusieurs moyens que nous
  vous décrivons ici !

* * *

## Configuration

[Quel pilote pour ma carte Wifi et comment le configurer ?](pilotes-wifi.html)
: Votre carte Wifi n'a pas de pilote, ne semble pas reconnue et vous
  voudriez bien sûr profiter du Wifi ? Cela sera très simple.

[Quel pilote pour ma carte graphique ?](pilotes-graphiques.html)
: Il existe souvent plusieurs pilotes pour votre carte graphique, des
  pilotes libres et des pilotes propriétaires et tout dépend de votre
  utilisation.

[Optimiser Linux](optimiser-linux.html)
: Vous voulez prendre soin de votre Linux, augmenter sa rapidité,
  contrôler sa RAM et l'utiliser au maximum, vider le cache, nettoyer les
  services de démarrage...

[Optimiser la batterie pour un laptop](optimiser-batterie.html)
: L'autonomie est importante pour tout le monde, et Linux peut faire des
  miracles s'il est correctement configuré. Ici, vous aurez des astuces
  diverses pour augmenter l'autonomie de votre ordinateur portable.

[Prendre soin de son matériel](prendre-soin-materiel.html) (CPU, SSD)
: Parce qu'un PC mérite aussi de la tendresse, nous vous expliquons
  comment prolonger sa durée de vie, surveiller l'état du matériel, etc.

* * *

## Aller plus loin

[Comment faire une bonne virtualisation ?](virtualisation.html)
: La virtualisation est l'art de faire tourner un système dans un autre
  (un Windows dans un Linux, un Linux dans un OSX, un Linux dans un
  Linux). Ceci est très pratique pour diverses choses, faire tourner un
  logiciel que l'on a pas sur son système, "avoir" 2 OS en même temps,
  tester une distribution, héberger un serveur particulier, etc.

[Que faire avec un Raspberry Pi ?](raspberry-pi.html)
: Le Raspberry Pi est un petit ordinateur de la taille d'une carte de
  crédit, tournant sous une nouvelle technologie de processeurs, et qui
  vous permettra d'y installer par exemple une distribution Linux pour en
  faire un serveur, un centre multimédia, un centre de gestion de caméras
  ou une simple station de travail très discrète.

[systemd](systemd.html) (Propriété de Richard_LeHap) **&gt;&gt; Pour quand ?**

[Quels sont les différents dossiers de la racine ?](hierarchy.html) &gt;&gt; | Knakis -- 2014/02/11 &gt; -__-'
